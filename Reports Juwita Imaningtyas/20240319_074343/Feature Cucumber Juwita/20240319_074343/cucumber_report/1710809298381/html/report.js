$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProductTanpaGambar.feature");
formatter.feature({
  "name": "Add a product without picture",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add a product without picture",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page without picture",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductTanpaGambar.navigateToAddProductWithoutPicture()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form without uploading the picture",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductTanpaGambar.fillFormsWithoutPicture()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get a notification to upload the picture",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductTanpaGambar.verifyProductPageWithoutPicture()"
});
formatter.result({
  "status": "passed"
});
});