package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class FeatureStep {

	@Given("User navigates to register page")
	def navigateToregsiter() {
	}

	@When("User fill the form")
	def enterform() {
		String randomString = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10)
		def email_code = ('ckks' + randomString) + '@gmail.com'

		def testData1 = [
			[('Nama') : 'Siong Tjie', ('Email') : '1'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : '!@#$%^', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') :  '2'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : '!@#$%^'],
			[('Nama') : '  Siong Tjie', ('Email') : '3'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : '!@#$%^', ('Email') : '4'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') : '5'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : ''],
			[('Nama') : 'Siong Tjie', ('Email') : '6'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : '', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') : '7'+email_code, ('Pass') : '31107010', ('Hp') : '', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') : '8'+email_code, ('Pass') : '', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') : '', ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : '', ('Email') : '9'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya'],
			[('Nama') : 'Siong Tjie', ('Email') : '10'+email_code, ('Pass') : '31107010', ('Hp') : '081233112532', ('Kota') : 'Surabaya', ('Alamat') : 'Surabaya']
		]

		for (def data : testData1) {
			def name = data['Nama']
			def email = data['Email']
			def pass = data['Pass']
			def hp = data ['Hp']
			def kota = data ['Kota']
			def alamat = data ['Alamat']


			Mobile.startApplication('C:\\Binar Academy\\Platinum Challenge\\secondhand-01022024.apk', true)
			Mobile.tap(findTestObject('Object Repository/Page_Login/Btn_akun'), 0)
			Mobile.tap(findTestObject('Object Repository/Page_Login/Btn_Masuk'), 0)
			Mobile.tap(findTestObject('Object Repository/Page_Register/Btn_Daftar_Awal'), 0)
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan nama lengkap'), name, 0)
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan email'), email, 0)
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan password'), pass, 0)
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan Nomor HP'), hp, 0)
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan kota'), kota, 0)
			Mobile.scrollToText('Alamat')
			Mobile.setText(findTestObject('Object Repository/Page_Register/Masukkan alamat'), alamat, 0)
			Mobile.tap(findTestObject('Object Repository/Page_Register/Btn_Daftar'), 0)
		}
	}

	@Then("User is navigated to profile")
	def verifyregister() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Page_Login/Btn_logout'), 0)
		Mobile.closeApplication()
	}
}