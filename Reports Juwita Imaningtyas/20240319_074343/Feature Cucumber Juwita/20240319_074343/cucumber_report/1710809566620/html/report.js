$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/RespondToOffer.feature");
formatter.feature({
  "name": "Respond to Offer Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User wants to accept an offer",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to notification page for accepting offers",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondToOffer.navigatesToAcceptOffer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accept an offer",
  "keyword": "When "
});
formatter.match({
  "location": "RespondToOffer.acceptOffer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on notification page after accepting",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondToOffer.verifyAccept()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User wants to reject an offer",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to notification page for rejecting offers",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondToOffer.navigatesToRejectOffer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User reject an offer",
  "keyword": "When "
});
formatter.match({
  "location": "RespondToOffer.rejectOffer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on notification page after rejecting",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondToOffer.verifyReject()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User wants to give accept offer and contact buyer via WA",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to notification page for contacting via WA",
  "keyword": "Given "
});
formatter.match({
  "location": "RespondToOffer.navigatesToContactWa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accept an offer and contact via WA",
  "keyword": "When "
});
formatter.match({
  "location": "RespondToOffer.contactWA()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on WA Page",
  "keyword": "Then "
});
formatter.match({
  "location": "RespondToOffer.verifyWa()"
});
formatter.result({
  "status": "passed"
});
});