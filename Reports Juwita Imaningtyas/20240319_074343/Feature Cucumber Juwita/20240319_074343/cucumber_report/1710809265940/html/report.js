$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProductHargaKosong.feature");
formatter.feature({
  "name": "Add a product with empty price",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add a product with empty price",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page with empty price",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductHargaKosong.navigateToAddProductEmptyPrice()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form without input price",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductHargaKosong.fillFormEmptyPrice()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get a notification to input the price",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductHargaKosong.verifyEmptyPrice()"
});
formatter.result({
  "status": "passed"
});
});