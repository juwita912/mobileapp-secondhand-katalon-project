package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddProductLimaLebih {

	@Given("User navigates to add product page to add first product")
	def navigatesToAddFirstProduct() {
		Mobile.startApplication('D:\\binar\\clone mobile apk\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}

	@When("User fills the form to add first product")
	def fillFormsFirstProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}

	@Then("User on a product page after add first product")
	def verifyFirstProduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_record/ViewGroup-verify element'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to add product page to add second product")
	def navigatesToAddSecondProduct() {
		Mobile.startApplication('D:\\binar\\clone juwita repo\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}

	@When("User fills the form to add second product")
	def fillFormsSecondProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}

	@Then("User on a product page after add second product")
	def verifySecondproduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_record/ViewGroup-verify element'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to add product page to add third product")
	def navigateToAddThirdProduct() {
		Mobile.startApplication('D:\\binar\\clone juwita repo\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}

	@When("User fills the form to add third product")
	def fillFormThirdProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}

	@Then("User on a product page after add third product")
	def verifyThirdProduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_record/ViewGroup-verify element'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to add product page to add fourth product")
	def navigatesToAddFourthProduct() {
		Mobile.startApplication('D:\\binar\\clone juwita repo\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}

	@When("User fills the form to add fourth product")
	def fillFormsFourthProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}
	@Then("User on a product page after add fourth product")
	def verifyFourthProduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_record/ViewGroup-verify element'), 0)

		Mobile.closeApplication()
	}
	@Given("User navigates to add product page to add fifth product")
	def navigatesToFifthProduct() {
		Mobile.startApplication('D:\\binar\\clone juwita repo\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}
	@When("User fills the form to add fifth product")
	def fillFormsFifthProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}
	@Then("User on a product page after add fifth product")
	def verifyFifthProduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_record/ViewGroup-verify element'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to add product page to add sixth product")
	def navigatesToSixthProduct() {
		Mobile.startApplication('D:\\binar\\clone juwita repo\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunkeempat@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/btn_icon_add product'), 0)
	}

	@When("User fills the form to add sixth product")
	def fillFormsSixthProduct() {
		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Nama Produk'), 'laptop', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Harga'), '100000', 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Spinner - Pilih Kategori'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/kategori-Komputer dan Aksesoris'), 0, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Lokasi Produk'), 'magelang', 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Deskripsi'), 'nice', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/ImageView Icon'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button-Image Galeri'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Pilih Image'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Add Product/Page_record/Button - Terbitkan'), 0)
	}

	@Then("User on a product page after add sixth product")
	def verifySixthProduct() {
		Mobile.verifyElementVisible(findTestObject('Page_Add Product/Page_spy/verify_TextView_Tambah Produk'), 0)

		Mobile.closeApplication()
	}
}