$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProduct.feature");
formatter.feature({
  "name": "Add product feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User wants to add a product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProduct.navigateToAddProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the forms",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.fillForm()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on product page",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.verifyAddProduct()"
});
formatter.result({
  "status": "passed"
});
});