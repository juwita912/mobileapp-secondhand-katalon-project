$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProductBanyakKategori.feature");
formatter.feature({
  "name": "Add a product with multiple categories",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add a product with multiple categories",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page with multiple categories",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductBanyakKategori.navigateToAddProductPageWithCategories()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form without input multiple categories",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductBanyakKategori.fillFormsWithCategories()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add a product with multiple categories",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductBanyakKategori.verifyMultipleCategories()"
});
formatter.result({
  "status": "passed"
});
});