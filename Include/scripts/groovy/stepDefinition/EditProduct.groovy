package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProduct {

	@Given("User navigates to edit product")
	def navigateToeditpage() {
		Mobile.startApplication('C:\\Binar Academy\\Platinum Challenge\\secondhand-01022024.apk', true)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_akun_awal produk'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_masuk produk'), 0)
		Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan email edit produk'), 'overlord.njoo@gmail.com',
				0)
		Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan password edit produk'), '31107010', 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Button - Masuk produk'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Daftar Jual Saya'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/ViewGroup produk 1'), 0)
	}

	@When("User fill form edit product")
	def entereditproduct() {

		Mobile.sendKeys(findTestObject('Page_Edit Produk/txt_nama_product'), 'testing 3', FailureHandling.STOP_ON_FAILURE)
		Mobile.sendKeys(findTestObject('Page_Edit Produk/txt_harga_product'), '200', FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Spinner - Pilih Kategori'), 0)
		Mobile.tap(findTestObject('Page_Edit Produk/kategori_hp_aksesoris'), 0)
		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
		Mobile.setText(findTestObject('Page_Edit Produk/android.widget.EditText - surabaya'), 'SURABAYA', 0)
		Mobile.setText(findTestObject('Page_Edit Produk/android.widget.EditText - surabaya 100'), 'surabaya 101', 0)
		Mobile.scrollToText('Deskripsi')
		Mobile.scrollToText('Foto Produk')
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Pilih_gambar'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Dari_galeri'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/menu 3 titik'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Images android'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Folder Picture android'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Gambar terpilih'), 0)
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Btn_update_produk'), 0)
	}

	@Then("User is navigated list product")
	def verifylisteditproduct() {

		Mobile.closeApplication()
	}
}