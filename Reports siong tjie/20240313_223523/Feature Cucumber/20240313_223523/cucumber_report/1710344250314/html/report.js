$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/USER/Katalon Studio/APK_Second_Hand/Include/features/Register.feature");
formatter.feature({
  "name": "Register Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to register",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to register page",
  "keyword": "Given "
});
formatter.match({
  "location": "FeatureStep.navigateToregsiter()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fill the form",
  "keyword": "When "
});
formatter.match({
  "location": "FeatureStep.enterform()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated to profile",
  "keyword": "Then "
});
formatter.match({
  "location": "FeatureStep.verifyregister()"
});
formatter.result({
  "status": "passed"
});
});