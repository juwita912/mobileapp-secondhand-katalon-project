Feature: Add a product without picture

Scenario: User want to add a product without picture
Given User navigates to add product page without picture
When User fills the form without uploading the picture
Then User get a notification to upload the picture
    
