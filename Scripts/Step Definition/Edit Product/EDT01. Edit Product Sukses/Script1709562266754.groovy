import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Binar Academy\\Platinum Challenge\\secondhand-01022024.apk', true)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_akun_awal produk'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_masuk produk'), 0)

Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan email edit produk'), 'overlord.njoo@gmail.com', 
    0)

Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan password edit produk'), '31107010', 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Button - Masuk produk'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Daftar Jual Saya'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/ViewGroup produk 1'), 0)

Mobile.sendKeys(findTestObject('Page_Edit Produk/txt_nama_product'), 'testing 3', FailureHandling.STOP_ON_FAILURE)

Mobile.sendKeys(findTestObject('Page_Edit Produk/txt_harga_product'), '200', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Spinner - Pilih Kategori'), 0)

Mobile.tap(findTestObject('Page_Edit Produk/kategori_hp_aksesoris'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Page_Edit Produk/android.widget.EditText - surabaya'), 'SURABAYA', 0)

Mobile.setText(findTestObject('Page_Edit Produk/android.widget.EditText - surabaya 100'), 'surabaya 101', 0)

Mobile.scrollToText('Deskripsi')

Mobile.scrollToText('Foto Produk')

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Pilih_gambar'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Dari_galeri'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/menu 3 titik'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Images android'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Folder Picture android'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Gambar terpilih'), 0)

Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Btn_update_produk'), 0)

not_run: Mobile.closeApplication()

