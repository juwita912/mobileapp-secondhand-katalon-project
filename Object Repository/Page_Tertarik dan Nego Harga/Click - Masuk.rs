<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Click - Masuk</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Button</value>
      <webElementGuid>53bdded0-8d69-41d4-8a74-aee4fb612cff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>6</value>
      <webElementGuid>9a03ee62-00a1-4f76-a7a8-5a9a085a8c49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Masuk</value>
      <webElementGuid>a9cdafe2-e021-4590-8245-7db30a200f11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand:id/btn_login</value>
      <webElementGuid>aafe8436-26f4-4ca6-8dd0-add0a56be277</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand</value>
      <webElementGuid>7ccf8a70-1bf4-419b-9ac7-1fdce8a2dace</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>63cd8c58-cf6f-4438-b405-4a4acd1964ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>96629b5b-e0da-404f-b8ae-c5404cc717c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>174212ac-db89-41c4-9699-4f318c74c1f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4804f914-ae40-422c-8b8a-5623ed683842</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>f3c7f2db-500a-464b-a6f5-1deda8267d25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>eed38b3d-ae94-4f36-9142-c9a5077f7b32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>1520fffe-08e6-412c-be9b-13396c06a9e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7815ef9f-70e6-4b2c-82f3-7eaa80a1e6e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>e8f466d0-bacd-43a2-8837-14e8e577358b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>61b72075-7064-4e5d-be77-79aa9ba893a3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>56</value>
      <webElementGuid>7bdebb6a-c822-44c8-b12d-c5b217c81098</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>1211</value>
      <webElementGuid>bb2b819f-6545-443b-9c73-157314a3b417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1328</value>
      <webElementGuid>984e0ab4-51ed-4d8e-8c15-9f7607e08a33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>194</value>
      <webElementGuid>75a17927-bdcc-45bd-ad4a-08eb85849d6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[56,1211][1384,1405]</value>
      <webElementGuid>35a0f3ac-9253-46f5-a6e7-f84b2dbf7d59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>fe9fd022-4d26-4548-af53-43aefcba22bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.widget.Button[1]</value>
      <webElementGuid>c8c5891d-47ec-4b94-a262-271ab62da7b8</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.Button' and (@text = 'Masuk' or . = 'Masuk') and @resource-id = 'id.binar.fp.secondhand:id/btn_login']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
